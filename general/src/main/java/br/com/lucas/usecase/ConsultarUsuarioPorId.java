package br.com.lucas.usecase;

import br.com.lucas.gateway.database.entity.Usuario;
import br.com.lucas.gateway.database.repository.UsuarioRepositoryFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ConsultarUsuarioPorId {

    private final UsuarioRepositoryFacade usuarioRepositoryFacade;

    public Usuario executar(int id){
        return usuarioRepositoryFacade.findById(id);
    }
}