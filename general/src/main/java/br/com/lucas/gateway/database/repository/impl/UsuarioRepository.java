package br.com.lucas.gateway.database.repository.impl;

import br.com.lucas.gateway.database.entity.Usuario;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
interface UsuarioRepository extends ObjectRepository<Usuario> {

    Optional<Usuario> findById(int id);

}