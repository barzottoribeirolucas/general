package br.com.lucas.gateway.database.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@Data @EqualsAndHashCode(of="id")
@AllArgsConstructor
public class Usuario {

    private int id;
    private String nome;
    private LocalDate dataNascimento;

}
