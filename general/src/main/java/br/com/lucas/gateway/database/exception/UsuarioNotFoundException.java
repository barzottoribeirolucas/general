package br.com.lucas.gateway.database.exception;

public class UsuarioNotFoundException extends NotFoundException {

    public UsuarioNotFoundException(String message) {
        super(message);
    }

}