package br.com.lucas.gateway.database.repository.impl;

import br.com.lucas.gateway.database.entity.Usuario;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Optional;

@Component
public class UsuarioRepositoryImpl implements UsuarioRepository{
    @Override
    public void store(Usuario usuario) {

    }

    @Override
    public Usuario retrieve(int id) {
        return null;
    }

    @Override
    public Usuario search(String name) {
        return null;
    }

    @Override
    public Usuario delete(int id) {
        return null;
    }

    @Override
    public Optional<Usuario> findById(int id) {
        return Optional.of(new Usuario(1, "Lucas", LocalDate.now()));
    }
}
