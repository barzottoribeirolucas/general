package br.com.lucas.gateway.database.repository;

import br.com.lucas.gateway.database.entity.Usuario;

public interface UsuarioRepositoryFacade {

    public Usuario findById(int id);
}
