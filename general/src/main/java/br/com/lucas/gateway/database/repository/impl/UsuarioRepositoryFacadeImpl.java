package br.com.lucas.gateway.database.repository.impl;

import br.com.lucas.gateway.database.entity.Usuario;
import br.com.lucas.gateway.database.exception.UsuarioNotFoundException;
import br.com.lucas.gateway.database.repository.UsuarioRepositoryFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioRepositoryFacadeImpl implements UsuarioRepositoryFacade {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Usuario findById(int id) {
        return usuarioRepository.findById(id)
                .orElseThrow(() -> new UsuarioNotFoundException(String.format("Usuário %d inexistente", id)));
    }
}