package br.com.lucas.http.domain.builder;

import br.com.lucas.domain.UserTO;
import br.com.lucas.gateway.database.entity.Usuario;
import org.springframework.stereotype.Component;

@Component
public class UserBuilder {

    public UserTO build (Usuario usuario) {
        if(usuario == null)
            return null;

        UserTO to = new UserTO();
        to.setId(usuario.getId());
        to.setName(usuario.getNome());
        to.setBirthdate(usuario.getDataNascimento());

        return to;
    }

}
