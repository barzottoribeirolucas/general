package br.com.lucas.http.domain.response;

import br.com.lucas.domain.UserTO;
import lombok.Data;

@Data
public class FindUserByIdResponse extends DefaultResponse{
    private UserTO user;
}