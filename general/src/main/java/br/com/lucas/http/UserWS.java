package br.com.lucas.http;

import br.com.lucas.domain.UserTO;
import br.com.lucas.gateway.database.entity.Usuario;
import br.com.lucas.http.domain.builder.UserBuilder;
import br.com.lucas.http.domain.response.FindUserByIdResponse;
import br.com.lucas.usecase.ConsultarUsuarioPorId;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@RequestMapping(path = ROOT_API_WS_USERS)
//@Api(tags = "Users", produces = APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class UserWS {

    private final ConsultarUsuarioPorId consultarUsuarioPorId;
    private final UserBuilder userBuilder;

    //@ApiOperation(value = "Find user by ID")
    @GetMapping("/users/{id}")
    public FindUserByIdResponse findById(@PathVariable("id") Integer id) {

        Usuario usuario = consultarUsuarioPorId.executar(id);
        UserTO userTO = userBuilder.build(usuario);
        FindUserByIdResponse response = new FindUserByIdResponse();
        response.setUser(userTO);

        return response;
    }

}