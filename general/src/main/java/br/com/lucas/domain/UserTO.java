package br.com.lucas.domain;

import lombok.Data;

import java.time.LocalDate;

@Data
public class UserTO {

    private int id;
    private String name;
    private LocalDate birthdate;
}